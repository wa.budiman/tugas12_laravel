<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\film;
use File;
class filmcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $film = DB::table('film')->get();
          $film = film::All();  
        return view('film.index', Compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $genre = DB::table('genre')->get();
        return view('film.create', Compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun'=>'required'
        ]);
        DB::table('film')->insert([
            'judul' => $request['judul'],
            'ringkasan' => $request['ringkasan'],
            'tahun' => $request['tahun'],
            'genre_id' => $request['genre_id']
        ]);
        return redirect('/film');*/

        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun'=>'required',
            'genre_id'=>'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $postername = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('gambar'),$postername);
        $film = new film();
        $film -> judul = $request->judul;
        $film -> ringkasan = $request->ringkasan;
        $film -> tahun = $request->tahun;
        $film -> genre_id = $request->genre_id;
        $film -> poster = $postername;

        $film->save();
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = film::findorfail($id);
        return view('film.show', Compact('film'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = film::findorfail($id);
        return view('film.edit', Compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun'=>'required',
            'genre_id'=>'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $film = film::find($id);
        if ($request->has('poster')){

            $postername = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('gambar'),$postername);
           
            $film -> judul = $request->judul;
            $film -> ringkasan = $request->ringkasan;
            $film -> tahun = $request->tahun;
            $film -> genre_id = $request->genre_id;
            $film -> poster = $postername;

        }else {
            $film -> judul = $request->judul;
            $film -> ringkasan = $request->ringkasan;
            $film -> tahun = $request->tahun;
            $film -> genre_id = $request->genre_id;
        }
        $film->save();
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $film = film::find($id);
        $path = 'gambar/';
        File::delete($path.$film->poster);
        $film->delete();
        return redirect('/film');
    }
}
