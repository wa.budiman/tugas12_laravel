@extends('layouts.master')

@section("Judul2")
    <h1> FILM {{$film->judul}}</h1>
@endsection

@section("content")
    <img src="{{asset('gambar/'.$film->poster)}}" alt="poster tidak ada">
    <h3> Produksi tahun : {{$film -> tahun}} <br> </h3>
    <h4> Sinopsis : <br> </h4>
    <p> {{$film -> ringkasan}} </p>
    
    <a href ='/film' class ='bnt btn-primary'> kembali </a>
@endsection