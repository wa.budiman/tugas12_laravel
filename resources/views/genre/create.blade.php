@extends('layouts.master')

@section("Judul2")
    <h1>GENRE Film</h1>
@endsection

@section("content")
    <form action ="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label >Nama</label>
            <input type="string" name ="nama" class="form-control">
      
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection